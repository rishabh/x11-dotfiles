# .bash_profile

export PATH="$HOME/bin:$PATH"
export EDITOR=kak
export BROWSER=chromium

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
